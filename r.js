let amqp = require('amqplib/callback_api');

amqp.connect('amqp://localhost', function(error, connect) {
  	
  	connect.createChannel(function(error, channel) {
  		channel.assertQueue('iambatman', {durable: false});
    
  		channel.consume('iambatman', function(message) {
			console.log('Recieve: '+message.content.toString());
		}, {noAck: true});
    
  	});

});

